const scores = [20, 50, 75, 100, 115];
const avg = array => array.reduce((acc, index) => {
    console.log('index', index)
    acc += index;
    return acc
}, 0) / array.length;

console.log(avg(scores));
//
const avg1 = (...array) => array.reduce((acc, index) => {
    console.log('index', index)
    acc += index;
    return acc
}, 0) / array.length;

console.log(Math.floor(avg1(50, 30, 40, 10, 5, 10, 20)));
//
const avg2 = (a, b, c) => {
    const p = (a + b + c) / 2
    const S = Math.sqrt(p*(p-a)*(p-b)*(p-c))
    return S
}
console.log('Площа',avg2(10,13,20))
//
const avg3 = array => array.map(index => {
        const arr1 = [];
        for (let i = 0; i < index; i++)
            arr1.push(Math.floor(Math.random() * (10 - 1)) + 1)
        return arr1;
    })

console.log('task4', avg3([1,2,3,4,5,6,7,8,9,10]))